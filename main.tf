##############
## Provider ##
##############

provider "aws" {
  region  = "us-east-2"

  access_key = "AKIA2V5XXBBAIJOSC7V4"
  secret_key = "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"
}

#########
## VPC ##  In this case of project we take the Default VPC
#########

#resource "aws_vpc" "mongodb-vpc" {
#  cidr_block = "10.0.0.0/16"
#
#  tags = {
#    Name = "km-aws-devops-vpc-hhe"
#  }
#}

####################
## Private Subnet ## In this case we take an existing subnet
####################

#resource "aws_subnet" "mongodb-subnet" {
#  vpc_id     = "vpc-09b03ef30108d104c"
#  subnet_id  = "subnet-06642aeeb8f991648"
#  #availability_zone = "us-east-2c"
#
#  tags = {
#    Name = "km-aws-devops-subnet-hhe"
#  }
#}

####################
## Security group ##
####################

#resource "aws_security_group" "mongodb-sg" {
#  name        = "allow_web_traffic"
#  description = "Allow Web inbound traffic"
#  vpc_id      = aws_vpc.mongodb-vpc.id
#
#  ingress {
#    description      = "HTTPS"
#    from_port        = 443
#    to_port          = 443
#    protocol         = "tcp"
#    cidr_blocks      = ["0.0.0.0/0"]
#  }
#
#  ingress {
#    description      = "HTTP"
#    from_port        = 80
#    to_port          = 80
#    protocol         = "tcp"
#    cidr_blocks      = ["0.0.0.0/0"]
#  }
#
#  ingress {
#    description      = "SSH"
#    from_port        = 22
#    to_port          = 22
#    protocol         = "tcp"
#    cidr_blocks      = ["0.0.0.0/0"]
#  }
#
#  egress {
#    from_port        = 0
#    to_port          = 0
#    protocol         = "-1"
#    cidr_blocks      = ["0.0.0.0/0"]
#   #ipv6_cidr_blocks = ["::/0"]
#  }
#
#  tags = {
#    Name = "km-aws-devops-sg-hhe"
#  }
#}


################################
## Create a network interface ##
################################
resource "aws_network_interface" "ni_hhe_primary" {
    subnet_id        = "subnet-06642aeeb8f991648"
    #private_ips      = ["172.31.16.2"]
    security_groups  = ["sg-0d87c8f49f474ff7b"]
    tags = {
        Name = "ni_hhe_primary"
    }
}

resource "aws_network_interface" "ni_hhe_secondary_1" {
    subnet_id        = "subnet-06642aeeb8f991648"
    #private_ips      = ["172.31.16.2"]
    security_groups  = ["sg-0d87c8f49f474ff7b"]
    tags = {
        Name = "ni_hhe_secondary_1"
    }
}

resource "aws_network_interface" "ni_hhe_secondary_2" {
    subnet_id        = "subnet-06642aeeb8f991648"
    #private_ips      = ["172.31.16.2"]
    security_groups  = ["sg-0d87c8f49f474ff7b"]
    tags = {
        Name = "ni_hhe_seconddary_2"
    }
}

resource "aws_network_interface" "ni_hhe_arbiter" {
    subnet_id        = "subnet-06642aeeb8f991648"
    #private_ips      = ["172.31.16.2"]
    security_groups  = ["sg-0d87c8f49f474ff7b"]
    tags = {
        Name = "ni_hhe_arbiter"
    }
}


###################
## Instances EC2 ##
###################


resource "aws_instance" "mongodb-primary" {
    #ami = "ami-051dfed8f67f095f5"
    #instance_type = "t2.large"
    ami = "ami-0960ab670c8bb45f3"
    instance_type = "t2.micro"
    #key_name = "kodemade-aws"
    key_name = "ubuntu-key"

    network_interface {
      network_interface_id = aws_network_interface.ni_hhe_primary.id
      device_index         = 0
    }
    tags = {
      Name = "km-aws-devops-hhe-primary"
    }
}


resource "aws_instance" "mongodb-secondary-1" {
    #ami = "ami-051dfed8f67f095f5"
    #instance_type = "t2.large"
    ami = "ami-0960ab670c8bb45f3"
    instance_type = "t2.micro"
    #key_name = "kodemade-aws"
    key_name = "ubuntu-key"

    network_interface {
      network_interface_id = aws_network_interface.ni_hhe_secondary_1.id
      device_index         = 0
    }
    tags = {
      Name = "km-aws-devops-hhe-secondary-1"
    }
}

resource "aws_instance" "mongodb-secondary-2" {
    #ami = "ami-051dfed8f67f095f5"
    #instance_type = "t2.large"
    ami = "ami-0960ab670c8bb45f3"
    instance_type = "t2.micro"
    #key_name = "kodemade-aws"
    key_name = "ubuntu-key"

    network_interface {
      network_interface_id = aws_network_interface.ni_hhe_secondary_2.id
      device_index         = 0
    }
    tags = {
      Name = "km-aws-devops-hhe-secondary-2"
    }
}



resource "aws_instance" "mongodb-arbiter" {
    #ami = "ami-051dfed8f67f095f5"
    #instance_type = "t2.large"
    ami = "ami-0960ab670c8bb45f3"
    instance_type = "t2.micro"
    #key_name = "kodemade-aws"
    key_name = "ubuntu-key"

    network_interface {
      network_interface_id = aws_network_interface.ni_hhe_arbiter.id
      device_index         = 0
    }
    tags = {
      Name = "km-aws-devops-hhe-arbiter"
    }
}

